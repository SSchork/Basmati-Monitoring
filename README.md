### UI (Angular App)
Before starting with Unicorn Angular app make sure that you have installed npm and nodejs.

Install AngularCLI 
```$shell
$ npm install -g @angular/cli
```
Install Angular Material

```$shell
$ npm install --save @angular/material @angular/cdk
```

Install Chart.js
```$shell
$ npm install chart.js --save
```


After installing necessary libraries, switch to DashboardUI folder

```shell
$ cd basmati_monitoring
```

Build and run Angular application by calling

```shell
$ ng serve
```

Open your browser at localhost:4200

### 