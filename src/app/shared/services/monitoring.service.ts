import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Service } from '../../layout/monitoring/Service';
import { AppConfig } from '../../app.config';

@Injectable()
export class MonitoringService {

  // private monitoringUrl = 'http://localhost:3000/service';
  // private monitoringUrl = 'https://cloud.amenesik.com/cobe/getprobedata.php?service=b1802f48-13fb-41eb-8a96-4a4171b59f7c';
  // private monitoringUrl = 'https://cloud.amenesik.com/cobe/getprobedata.php?service=6528151e-9b49-4ffb-a375-40f1083342fe';
  // private monitoringUrl =  'http://basmati-ace-france.foghopper.com/cobe/getprobedata.php?service=bee0996f-73c7-46ae-ba40-c95deb9c44e6';
  // private monitoringUrl = 'https://basmati-ace-france.foghopper.com/cobe/getprobedata.php?service=e7479f00-6600-4a1b-b1c3-d20295d9c003';

  protected apiData = AppConfig.settings.apiServer.metadata;

  constructor(private http: HttpClient) { }

  getMonitoringData(): Observable<string> {

    return this.http.get(this.apiData, {responseType: 'text'});

    // return this.http.get<Service>(this.monitoringUrl);
   }

}
