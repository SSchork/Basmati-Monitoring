import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';

export class InMemoryAppService implements InMemoryDbService {

  createDb() {
    const apps = [

      {id: 11, appName: 'MyJavaApp', appOwner: 'juliav', appHost: 'aws', appError: 'critical' },
      {id: 21, appName: 'MyService', appOwner: 'testuser', appHost: 'aws', appError: 'medium' },
      {id: 31, appName: 'FirstMS', appOwner: 'testuser', appHost: 'aws', appError: 'none' },
      {id: 41, appName: 'TestApp', appOwner: 'juliav', appHost: 'aws', appError: 'critical' }

  ];

  return {apps};

  }

}
