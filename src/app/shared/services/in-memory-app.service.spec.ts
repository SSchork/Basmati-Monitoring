import { TestBed, inject } from '@angular/core/testing';

import { InMemoryAppService } from './in-memory-app.service';

describe('InMemoryAppService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [InMemoryAppService]
    });
  });

  it('should be created', inject([InMemoryAppService], (service: InMemoryAppService) => {
    expect(service).toBeTruthy();
  }));
});
