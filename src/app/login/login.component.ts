import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: []
})
export class LoginComponent implements OnInit {

  model: any = {};
  loading = false;
  error = '';

  constructor(private router: Router) { }

  ngOnInit() {}

  login () {
    this.router.navigate(['/dashboard']);
  }

  goToRegistrationPage () {
    this.router.navigate(['registration']);
  }

}

