export class Limits {

    name: string;
    condition: string;
    value: number;
    period: number;

    constructor (name: string, condition: string, value: number, period: number) {
        this.name = name;
        this.condition = condition;
        this.value = value;
        this.period = period;
    }

}
