import { Activity } from './Activity';

export class Contract {

   id: number;
   name: string;
   activity: Activity[];
   status: string;
   background_color: string;

   constructor(activity: Activity[], id: number, name: string, status: string, background_color: string) {
       this.id = id;
       this.name = name;
       this.activity = activity;
       this.status = status;
       this.background_color = background_color;
   }
}
