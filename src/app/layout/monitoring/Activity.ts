export class Activity {

    metric: string;
    start: number;
    data: string;

    constructor(data: any, metric: string, start: number) {
        this.metric = metric;
        this.start = start;
        this.data = data;
    }
}
