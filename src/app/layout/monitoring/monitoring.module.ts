import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MonitoringRoutingModule } from './monitoring-routing.module';
import { MonitoringComponent } from './monitoring.component';
import { MaterialModule } from '../../material.module';

@NgModule({
  imports: [
    CommonModule,
    MonitoringRoutingModule,
    MaterialModule
  ],
  declarations: [MonitoringComponent]
})
export class MonitoringModule { }
