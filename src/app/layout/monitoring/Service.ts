import { Contracts } from './Contracts';
import { Limits } from './Limits';

export class Service {

    contracts: Contracts[];
    id: number;
    name: string;
    limits: Limits[];


    constructor(contracts: Contracts[], id: number, name: string, limits: Limits[]) {
        this.id = id;
        this.name = name;
        this.contracts = contracts;
        this.limits = limits;
    }
}
