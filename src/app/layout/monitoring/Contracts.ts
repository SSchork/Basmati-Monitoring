import { Contract } from './Contract';

export class Contracts {

    contract: Contract;

    constructor (contract: Contract) {
        this.contract = contract;
    }
}
