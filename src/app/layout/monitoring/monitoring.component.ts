import { Component, OnInit, OnDestroy, OnChanges, SimpleChange, Input, SimpleChanges } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { MonitoringService } from '../../shared/services/monitoring.service';
import { Service } from './Service';
import { Services } from './Services';
import { Activity } from './Activity';
import { Chart } from 'chart.js';
import { Limits } from './Limits';

@Component({
  selector: 'app-monitoring',
  templateUrl: './monitoring.component.html',
  styleUrls: ['./monitoring.component.scss'],
  animations: [routerTransition()],
  providers: [MonitoringService]
})
export class MonitoringComponent implements OnInit, OnDestroy {
  contractId: number;
  sub: any;
  monitoringData: Activity[];
  chartCpu = [];
  chart = [];
  chartDataCpu = [];
  chartData = [];
  chartLabelsCpu = [];
  chartLabels = [];
  metrices = [];
  service: Service;
  services: Services;
  monData: any;
  charttitle: string;
  date: Date;
  yAxesScales = [];
  limitData = [];
  currLimit: Number;

  constructor(private router: Router,
              private route: ActivatedRoute,
              private monitoringService: MonitoringService) { }

  ngOnInit() {

    this.sub = this.route.params.subscribe(params => {this.contractId = params['contractId'];
    });

    // get monitoring data to specific contract ID
    this.monitoringService.getMonitoringData().subscribe(
      data => {
        // this.service = data;
        // repair data and parse to JSON as a workaround
        this.service = this.parseToJSON(data);

        // this.service = this.services.service;

        // retrieve monitoringData
        this.monitoringData = this.retrieveMonitoringData(this.service);

        // find all metrices
        this.metrices = this.countMetrices(this.monitoringData);

        // create memory chart
        for (let i = 0; i < this.metrices.length; ++i) {
          // compute current limit
          this.currLimit = this.computeCurrentLimit(this.metrices[i], this.service.limits);

          this.createChart(this.monitoringData, this.metrices[i], this.chartData, this.chartLabels, this.chart,
            this.currLimit, this.limitData);
        }
      }
    );
  }
  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

  // parse to JSON function
  parseToJSON(data: string): Service {
    this.monData = data.replace(/\\n/g, '\\n')
            .replace(/\\'/g, '\\')
            .replace(/\\"/g, '\\"')
            .replace(/\\&/g, '\\&')
            .replace(/\\r/g, '\\r')
            .replace(/\\t/g, '\\t')
            .replace(/\\b/g, '\\b')
            .replace(/\\f/g, '\\f');
    this.monData = this.monData.replace(/[\u0000-\u0019]+/g, '');
    this.service = JSON.parse(this.monData);
    return this.service;

  }

  // retrieve monitoring data
  retrieveMonitoringData(service: Service): Activity[] {
    for (let i = 0; i < this.service.contracts.length; i++) {
      if (this.service.contracts[i].contract.id === this.contractId) {
        this.monitoringData = this.service.contracts[i].contract.activity;
      }
    }
    return this.monitoringData;
  }

  // count metrices
  countMetrices(monitoringData: Activity[]): any {
        for (let i = 0; i < this.monitoringData.length; i++) {
          if (this.metrices.indexOf(this.monitoringData[i].metric) === -1) {
            this.metrices.push(this.monitoringData[i].metric);
          }
        }
        return this.metrices;
  }

  // compute current limit
  computeCurrentLimit(metric: string, limits: Limits[]): Number {

    let currentLimit = 0;

    // loop over all limits to find the right value
    for (let i = 0; i < limits.length; i++) {
      if (metric === limits[i].name) {
        currentLimit = limits[i].value;
      }
    }
    return currentLimit;
  }

  // create charts
  createChart(monitoring: Activity[], metric: string, chartData: any, chartLabels: any,
    chart: any, limit: Number, limitData: any): Chart {

      chartData = [];
      chartLabels = [];
      chart = [];
      limitData = [];
      // compute monitoring data to be displayed
    for (let i = 0; i < monitoring.length; ++i) {
      if (monitoring[i].metric === metric) {
        // push data to data vector
        chartData.push(monitoring[i].data);
        // push time stamp to labelvector
        chartLabels.push((new Date(Number(monitoring[i].start) + 1514761200)).toTimeString().slice(0, 8));
        // chartLabels.push(new Date().toTimeString().slice(0, 8) );
        // push limit to limitvector
        limitData.push(limit);
      }
    }
    chartLabels.reverse();
    chartData.reverse();

    console.log(limitData);

    // compute charttitle
    if (metric === 'memory:free') {
      this.charttitle = 'Free Memory';
      this.yAxesScales = [{
        display: true,
        scaleLabel: {
          display: true,
          labelString: 'free RAM for current period'}
      }];
    } else if (metric === 'cpu:user') {
      this.charttitle = 'CPU: User';
      this.yAxesScales = [{
        display: true,
        ticks: {min: 0, max: 10},
        scaleLabel: {
          display: true,
          labelString: 'ni'}
      }];
    } else if (metric === 'disk:free') {
      this.charttitle = 'Disk';
      this.yAxesScales = [{
        display: true,
        scaleLabel: {
          display: true,
          labelString: 'free diskspace'}
      }];
    } else if (metric === 'net:eth0:tx') {
      this.charttitle = 'Network: tx';
      this.yAxesScales = [{
        display: true,
        scaleLabel: {
          display: true,
          labelString: 'CPU usage'}
      }];
    } else if (metric === 'net:eth0:rx') {
      this.charttitle = 'Network: rx';
      this.yAxesScales = [{
        display: true,
        scaleLabel: {
          display: true,
          labelString: 'network reception total in GB'}
      }];
    }

    chart = new Chart (metric, {
      type: 'line',
      data: {
        labels: chartLabels,
        datasets: [
          // monitorint data displayed
          {
            data: chartData,
            borderColor: '#3cba9f',
            fill: false,
            label: metric
          },
          // display limit value as constant line
          {
            data: limitData,
            borderColor: '#FA5882',
            fill: false
          }
        ]
      },
      options: {
        title: {
          display: true,
          text: this.charttitle,
          fontSize: 18
        },
        legend: {
          display: false
        },
        scales: {
          xAxes: [{
            display: true
          }],
          yAxes: this.yAxesScales
        }
      }
    });

  }

}
