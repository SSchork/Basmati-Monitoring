import { Service } from './Service';

export class Services {

    service: Service;

    constructor(service: Service) {
        this.service = service;
    }
}
