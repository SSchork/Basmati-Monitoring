import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { MonitoringService } from '../../../shared/services/monitoring.service';
import { Services } from '../../monitoring/Services';
import { Service } from '../../monitoring/Service';
import { Contract } from '../../monitoring/Contract';

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss'],
    providers: [MonitoringService]
})
export class SidebarComponent implements OnInit {
    isActive: boolean = false;
    showMenu: string = '';
    pushRightClass: string = 'push-right';
    monData: any;
    machineData: any;
    service: Service;
    services: Services;

    constructor(private translate: TranslateService,
                public router: Router,
                private monService: MonitoringService) {
        this.translate.addLangs(['en', 'fr', 'ur', 'es', 'it', 'fa', 'de']);
        this.translate.setDefaultLang('en');
        const browserLang = this.translate.getBrowserLang();
        this.translate.use(browserLang.match(/en|fr|ur|es|it|fa|de/) ? browserLang : 'en');

        this.router.events.subscribe(val => {
            if (
                val instanceof NavigationEnd &&
                window.innerWidth <= 992 &&
                this.isToggled()
            ) {
                this.toggleSidebar();
            }
        });
    }

      ngOnInit() {
      // fetch data from monitoring api
        this.monService.getMonitoringData().subscribe(data => {
            // repair hidden characters in JSON
            this.monData = data.replace(/\\n/g, '\\n')
            .replace(/\\'/g, '\\')
            .replace(/\\"/g, '\\"')
            .replace(/\\&/g, '\\&')
            .replace(/\\r/g, '\\r')
            .replace(/\\t/g, '\\t')
            .replace(/\\b/g, '\\b')
            .replace(/\\f/g, '\\f');

            this.monData = this.monData.replace(/[\u0000-\u0019]+/g, '');
            this.service = JSON.parse(this.monData);

            // this.service = data;
            this.machineData = this.service.contracts;
        });
    }

    eventCalled() {
        this.isActive = !this.isActive;
    }

    addExpandClass(element: any) {
        if (element === this.showMenu) {
            this.showMenu = '0';
        } else {
            this.showMenu = element;
        }
    }

    isToggled(): boolean {
        const dom: Element = document.querySelector('body');
        return dom.classList.contains(this.pushRightClass);
    }

    toggleSidebar() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle(this.pushRightClass);
    }

    rltAndLtr() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle('rtl');
    }

    changeLang(language: string) {
        this.translate.use(language);
    }

    onLoggedout() {
        localStorage.removeItem('isLoggedin');
    }

    goToMonPage(contract: Contract) {
        this.router.navigate(['/monitoring', contract.id]);
    }
}
