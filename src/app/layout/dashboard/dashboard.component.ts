import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router} from '@angular/router';
import { HttpClient} from '@angular/common/http';

import { MonitoringService } from '../../shared/services/monitoring.service';
import { Service } from '../monitoring/Service';
import { Services } from '../monitoring/Services';
import { Contract } from '../monitoring/Contract';
import { Limits } from '../monitoring/Limits';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss'],
    animations: [routerTransition()]
})
export class DashboardComponent implements OnInit {

    machineData: any = [];
    criticalContracts: any = [];
    warningContracts: any = [];
    okContracts: any = [];
    machineNames: String[];
    machineIds: number[];
    service: Service;
    services: Services;
    monData: any;
    tile = [];
    serviceName: string;
    errorMessage: string;


    constructor(private monService: MonitoringService,
                private router: Router,
            private http: HttpClient) {}

    ngOnInit() {
        // fetch data from monitoring api
        this.monService.getMonitoringData().subscribe(data => {
            // repair hidden characters in JSON
            this.monData = data.replace(/\\n/g, '\\n')
            .replace(/\\'/g, '\\')
            .replace(/\\"/g, '\\"')
            .replace(/\\&/g, '\\&')
            .replace(/\\r/g, '\\r')
            .replace(/\\t/g, '\\t')
            .replace(/\\b/g, '\\b')
            .replace(/\\f/g, '\\f');

            this.monData = this.monData.replace(/[\u0000-\u0019]+/g, '');
            this.service = JSON.parse(this.monData);
            this.serviceName = this.service.name;
            this.computeStatusOfMachine(this.service);
            this.computeContractLists(this.service);

            console.log(this.service);
        },
        err => {
            this.errorMessage = 'Service not available';

        });

        /* this.monService.getMonitoringData().subscribe(
            data => {
                this.service = data;
                this.machineData = this.service.contracts;
                this.serviceName = this.service.name;

                // set status of monitored apps
                for (let i = 0; i < this.machineData.length; i++) {
                    this.machineData[i].contract.status = 'critical';
                }

            },
            err => {
                this.errorMessage = 'Service not available';

            }
        );*/
    }

    goToMonPage(contract: Contract) {
        this.router.navigate(['/monitoring', contract.id]);
    }

    computeStatusOfMachine(service: Service) {
        console.log(service.limits);

        // loop over all contracts
        for (let i = 0; i < service.contracts.length; i++) {

            const m = service.contracts[i].contract.activity.length - 2;
            // console.log(m);
            // console.log (service.contracts[i].contract.activity.length);

            // loop over all activities
            for (let j = 0; j < service.contracts[i].contract.activity.length; j++) {

                if (service.contracts[i].contract.status !== 'critical') {

                // first step by hand
                switch (service.contracts[i].contract.activity[j].metric) {

                    case 'cpu:user':
                    if (Number(service.contracts[i].contract.activity[j].data) > service.limits[0].value) {
                        service.contracts[i].contract.background_color = '#FA5882';
                        service.contracts[i].contract.status = 'critical';
                    } else {
                        service.contracts[i].contract.background_color = '#81F781';
                        service.contracts[i].contract.status = 'ok';
                    }
                    break;

                    case 'memory:free':
                    if (Number(service.contracts[i].contract.activity[j].data) < service.limits[4].value) {
                        service.contracts[i].contract.background_color = '#FA5882';
                        service.contracts[i].contract.status = 'critical';
                    } else {
                        service.contracts[i].contract.background_color = '#81F781';
                        service.contracts[i].contract.status = 'ok';
                    }

                    break;

                    case 'disk:free':
                    if (Number(service.contracts[i].contract.activity[j].data) < service.limits[2].value) {
                        service.contracts[i].contract.background_color = '#FA5882';
                        service.contracts[i].contract.status = 'critical';
                    } else {
                        service.contracts[i].contract.background_color = '#81F781';
                        service.contracts[i].contract.status = 'ok';
                    }
                    break;

                    case 'net:eth0:tx':
                    if (Number(service.contracts[i].contract.activity[j].data) > service.limits[1].value) {
                        service.contracts[i].contract.background_color = '#FA5882';
                        service.contracts[i].contract.status = 'critical';
                    } else {
                        service.contracts[i].contract.background_color = '#81F781';
                        service.contracts[i].contract.status = 'ok';
                    }
                    break;

                    case 'net:etho:rx':
                    if (Number(service.contracts[i].contract.activity[j].data) > service.limits[3].value) {
                        service.contracts[i].contract.background_color = '#FA5882';
                        service.contracts[i].contract.status = 'critical';
                    } else {
                        service.contracts[i].contract.background_color = '#81F781';
                        service.contracts[i].contract.status = 'ok';
                    }
                    break;

                }
            }

            }

        }


    }

    computeContractLists(service: Service) {
        // loop over all contracts
        for (let i = 0; i < service.contracts.length; i++) {
            console.log(service.contracts[i].contract.status);
            console.log(service.contracts[i].contract.background_color);
            switch (service.contracts[i].contract.status) {
                case 'critical':
                this.criticalContracts.push(service.contracts[i].contract);
                break;

                case 'warning':
                this.warningContracts.push(service.contracts[i].contract);
                break;

                case 'ok':
                this.okContracts.push(service.contracts[i].contract);
                break;
            }
        }
    }
}
