import { Injectable } from '@angular/core';
import { IAppConfig } from './app-config.model';
import { environment } from '../environments/environment';
import { Http, Response } from '@angular/http';

@Injectable()
export class AppConfig {

    static settings: IAppConfig;

    constructor(private http: Http) {}

    load() {
        const jsonFile = `assets/config/config.${environment.name}.json`;

        return new Promise<void>((resolve, reject) => {
            this.http.get(jsonFile).toPromise().then((resp: Response) => {
                AppConfig.settings = <IAppConfig>resp.json();
                resolve();
            }).catch((resp: any) => {
                reject(`Could not load file '${jsonFile}': ${JSON.stringify(resp)}`);
            });
        });
    }

}
