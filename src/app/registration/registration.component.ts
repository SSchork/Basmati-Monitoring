import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {

  registerMessage: String;
  registerForm: FormGroup;

  constructor(private router: Router) { }

  ngOnInit() {
        // create new user form
        this.registerForm = new FormGroup({
          password: new FormControl('', Validators.required),
          email: new FormControl('', [Validators.required, Validators.pattern('[^@]*@[^ @]*')]),
          fullName: new FormControl('', Validators.required),
          username: new FormControl('', Validators.required)
        });
  }


  goToLoginPage () {
    this.router.navigate(['/dashboard']);

  }

  registerUser() {
    this.router.navigate(['/dashboard']);
  }

}
